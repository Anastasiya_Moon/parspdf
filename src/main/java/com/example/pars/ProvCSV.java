package com.example.pars;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

class ProvCSV {
    //проверка: если файл .csv отсутствует, то он его создаёт
    void prov(String paths) throws IOException {
        if (!Files.exists(Paths.get(paths))) {
            try {
                new PrintWriter(new File(paths));
                //именуем столбцы в файле
                String nameTableColumn = "\"" + "TITLE" + "\";" + "\"" + "FIRST NAME" + "\";" + "\"" + "SURNAME" + "\";" + "\""
                        + "COMPANY NAME" + "\";" + "\"" + "STREET ADDRESS" + "\";" + "\"" + "TELEPHONE NUMBER" + "\";" + "\""
                        + "MOBILE NUMBER" + "\";" + "\"" + "TOWN_CITY" + "\";" + "\"" + "FAX_NUMBER" + "\";" + "\"" + "COUNTRY"
                        + "\";" + "\"" + "POSTCODE" + "\";" + "\"" + "EMAIL_ADDRESS" + "\";" + "\"" + "TITLE2" + "\";" + "\""
                        + "FIRST NAME2" + "\";" + "\"" + "SURNAME2" + "\";" + "\""
                        + "COMPANY NAME2" + "\";" + "\"" + "STREET ADDRESS2" + "\";" + "\"" + "TELEPHONE NUMBER2" + "\";" + "\""
                        + "MOBILE NUMBER2" + "\";" + "\"" + "TOWN_CITY2" + "\";" + "\"" + "FAX_NUMBER2" + "\";" + "\"" + "COUNTRY2"
                        + "\";" + "\"" + "POSTCODE2" + "\";" + "\"" + "EMAIL_ADDRESS2" + "\";" + "\"" + "PROPOSED" + "\";" + "\""
                        + "HOUSE" + "\";" + "\"" + "SUFFIX" + "\";" + "\"" + "HOUSENAME" + "\";" + "\""
                        + "STREETADDRESS3" + "\";" + "\"" + "TOWN_CITY3" + "\";" + "\"" + "POSTCODE3" + "\";" + "\"" + "EASTING"
                        + "\";" + "\"" + "NORTHING" + "\";" + "\"" + "DESCRIPTION" + "\";" + "\n";
                //записываем в csv
                Files.write(Paths.get(paths), nameTableColumn.getBytes(), StandardOpenOption.APPEND);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}