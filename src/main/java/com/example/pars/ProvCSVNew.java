package com.example.pars;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

class ProvCSVNew {
    //проверка: если файл .csv отсутствует, то он его создаёт
    void prov(String paths) throws IOException {
        if (!Files.exists(Paths.get(paths))) {
            try {
                new PrintWriter(new File(paths));
                //именуем столбцы в файле
                String nameTableColumn = "\"" + "Number" + "\";" + "\"" + "Suffix" + "\";" + "\"" + "PropertyName" + "\";" + "\""
                        + "AddressLine1" + "\";" + "\"" + "AddressLine2" + "\";" + "\"" + "AddressLine3" + "\";" + "\""
                        + "Town_city" + "\";" + "\"" + "Postcode" + "\";" + "\"" + "Easting" + "\";" + "\"" + "Northing"
                        + "\";" + "\"" + "Description" + "\";" + "\"" + "Title" + "\";" + "\"" + "FirstName" + "\";" + "\""
                        + "Surname" + "\";" + "\"" + "CompanyName" + "\";" + "\""
                        + "AddressLine1A" + "\";" + "\"" + "AddressLine2A" + "\";" + "\"" + "AddressLine3A" + "\";" + "\""
                        + "Town_cityA" + "\";" + "\"" + "Country" + "\";" + "\"" + "PostcodeA" + "\";" + "\"" + "PrimaryNumber"
                        + "\";" + "\"" + "SecondaryNumber" + "\";" + "\"" + "FaxNumber" + "\";" + "\"" + "EmailAddress" + "\";"
                        + "\"" + "Title2" + "\";" + "\"" + "FirstName2" + "\";" + "\""
                        + "Surname2" + "\";" + "\"" + "CompanyName2" + "\";" + "\""
                        + "AddressLine1A2" + "\";" + "\"" + "AddressLine2A2" + "\";" + "\"" + "AddressLine3A2" + "\";" + "\""
                        + "Town_cityA2" + "\";" + "\"" + "Country2" + "\";" + "\"" + "PostcodeA2" + "\";" + "\"" + "PrimaryNumber2"
                        + "\";" + "\"" + "SecondaryNumber2" + "\";" + "\"" + "FaxNumber2" + "\";" + "\"" + "Email" + "\";"
                        + "\"" + "Decision" + "\";" + "\"" + "ReferenceNumbe" + "\";" + "\"" + "Date" + "\";" + "\"" + "NameFile" + "\";" + "\n";
                //записываем в csv
                Files.write(Paths.get(paths), nameTableColumn.getBytes(), StandardOpenOption.APPEND);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}