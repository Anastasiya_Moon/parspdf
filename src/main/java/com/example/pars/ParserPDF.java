package com.example.pars;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

public class ParserPDF {
    private static final String PATH = "testCSV/";
    static final String PATHcsvO = PATH + "/List_pars_old.csv";
    static final String PATHcsvN = PATH + "/List_pars_new.csv";
    private static ProvCSV provCSV = new ProvCSV();
    private static ProvCSVNew provCSVNew = new ProvCSVNew();
    static String[] str = new String[35];
    static String[] strN = new String[43];


    // Проверить, содержится ли pattern в examinee
    private static boolean contains(String examinee, String pattern) {
        if (pattern.length() > examinee.length()) return false;
        String s1 = " " + examinee + " ";    // гарантируем непустую строку слева и справа
        String s2 = "\\Q" + pattern + "\\E"; // Трактовать как литерал
        if (s1.split(s2).length > 1) return true; // что-то есть и слева, и справа
        return false;
    }

    //находим номер строки где находим нужную подстроку
    private static int seachInd(Integer i, String[] ary1, String str) {
        Integer j = 0;
        for (String str1 = ary1[i]; i < ary1.length - 1; i++, str1 = ary1[i]) {
            //System.out.println(str1);
            if (contains(str1, str)) {
                j = i;
                break;
            } else j = -1;
        }
        //System.out.println(j);
        return j;
    }

    private static int indexStart(String str, String podstr) {
        int index = str.lastIndexOf(podstr);
        return index;
    }

    public static void main(String[] args) throws IOException {
        //фиксируем начальное время
        long start = System.currentTimeMillis();
        int kolF = 0;
        int ii;
        String path;
        //расположение файлов .pdf
        File folder = new File("pdf/");
        File[] listOfFiles = folder.listFiles();

        //проверка на существование файла .csv
        provCSV.prov(PATHcsvO);
        provCSVNew.prov(PATHcsvN);
        //проходим по всем файлам pdf в папке
        for (File file : listOfFiles) {
            if (file.isFile()) {
                System.out.println(file.getName());
                path = "pdf/" + file.getName();
                //path = "pdf/18_01831_FUL-APPLICATIONFORMNOPERSONALDATA-626943.pdf";

                try {
                    InputStream input = new FileInputStream(new File(path));
                    ContentHandler textHandler = new BodyContentHandler();
                    Metadata metadata = new Metadata();
                    AutoDetectParser parser = new AutoDetectParser();
                    ParseContext context = new ParseContext();
                    parser.parse(input, textHandler, metadata, context);
//            System.out.println("Title: " + metadata.get(metadata.TITLE));

                    //получаем весь текст документа
                    String text = textHandler.toString();
//            System.out.println("Body: " + text);

                    String[] ary = text.split("\n");
                    if (ary.length < 1)
                        System.out.println("empty");
                    else {
                        kolF++;
                        System.out.println("not empty");
                        //проверяем по содержанию файл(и выбираем тип сбора информации)
                        if (seachInd(0, ary, "1. Applicant Name") >= 0) {
                            System.out.println("old type!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                            String TITLE1, FIRSTNAME, SURNAME, COMPANYNAME, STREETADDRESS, TELEPHONENUMBER, MOBILENUMBER, TOWN_CITY,
                                    FAX_NUMBER, COUNTRY, POSTCODE, EMAIL_ADDRESS, PROPOSED, HOUSE, SUFFIX, HOUSENAME, STREETADDRESS3,
                                    TOWN_CITY3, POSTCODE3, EASTING, NORTHING, DESCRIPTION;

                            System.out.println("1. Applicant Name, Address and Contact Details: ");

                            Integer i = seachInd(0, ary, "1. Applicant Name");

                            i = i + 2;//15
                            i = seachInd(i, ary, "Title:");
                            ii = seachInd(i, ary, "First Name:");
                            System.out.println(ary[i]);
                            if(i != ii) {
                                TITLE1 = ary[i].substring(ary[i].indexOf(':') + 1);
                                i++;
                               // System.out.println(ary[i]);
                                while (i < ii) {
                                    TITLE1 += ' ' + ary[i];
                                    i++;
                                }
                                i = ii;
                            }
                            else {
                                TITLE1 = ary[i].substring(ary[i].indexOf(':') + 1, ary[i].lastIndexOf("First Name"));

                            }
                            System.out.println("TITLE1: " + TITLE1);
                            str[0] = TITLE1;
                            System.out.println(ary[i]);
                            FIRSTNAME = ary[i].substring(indexStart(ary[i], "First Name:") + 11, indexStart(ary[i], "Surname:"));
                            System.out.println("FIRST NAME: " + FIRSTNAME);
                            str[1] = FIRSTNAME;

                            SURNAME = ary[i].substring(ary[i].lastIndexOf(':') + 1);
                            System.out.println("SURNAME: " + SURNAME);
                            str[2] = SURNAME;

                            i = i + 2;//17
                            COMPANYNAME = ary[i].substring(ary[i].indexOf(':') + 1);
                            System.out.println("COMPANY NAME: " + COMPANYNAME);
                            str[3] = COMPANYNAME;

                            i = i + 2;//19
                            STREETADDRESS = ary[i].substring(ary[i].indexOf(':') + 1) + ary[i + 2].substring(0, ary[i + 2].lastIndexOf("Telephone ")) + ary[i + 4].substring(0, ary[i + 4].lastIndexOf("Mobile number:"));
                            System.out.println("STREET ADDRESS: " + STREETADDRESS);
                            str[4] = STREETADDRESS;

                            i = i + 2;//21
                            TELEPHONENUMBER = ary[i].substring(ary[i].indexOf(':') + 1);
                            System.out.println("TELEPHONE NUMBER: " + TELEPHONENUMBER);
                            str[5] = TELEPHONENUMBER;

                            i = i + 2;//23
                            MOBILENUMBER = ary[i].substring(ary[i].indexOf(':') + 1);
                            System.out.println("MOBILE NUMBER: " + MOBILENUMBER);
                            str[6] = MOBILENUMBER;

                            i = i + 2;//25
                            TOWN_CITY = ary[i].substring(ary[i].indexOf(':') + 1, ary[i].lastIndexOf("Fax number:"));
                            System.out.println("TOWN/CITY: " + TOWN_CITY);
                            str[7] = TOWN_CITY;
                            FAX_NUMBER = ary[i].substring(ary[i].lastIndexOf(':') + 1);
                            System.out.println("FAX NUMBER: " + FAX_NUMBER);
                            str[8] = FAX_NUMBER;

                            i = i + 2;//27
                            COUNTRY = ary[i].substring(ary[i].indexOf(':') + 1);
                            System.out.println("COUNTRY: " + COUNTRY);
                            str[9] = COUNTRY;

                            i = i + 4;//31
                            Integer lan = ary[i].length();
                            if (lan > 17) lan = 18;
                            POSTCODE = ary[i].substring(ary[i].indexOf(':') + 1, lan);
                            System.out.println("POSTCODE: " + POSTCODE);
                            str[10] = POSTCODE;
                            EMAIL_ADDRESS = ary[i].substring(lan);
                            System.out.println("EMAIL ADDRESS: " + EMAIL_ADDRESS);
                            str[11] = EMAIL_ADDRESS;

                            i = seachInd(0, ary, "2. Agent Name,");
                            System.out.println("2. Agent Name, Address and Contact Details ");

                            if (seachInd(i, ary, "First Name") > 0 )
                            {
                                i = i + 2;//15
                                TITLE1 = ary[i].substring(ary[i].indexOf(':') + 1, ary[i].lastIndexOf("First Name"));
                                System.out.println("TITLE1: " + TITLE1);
                                str[12] = TITLE1;

                                FIRSTNAME = ary[i].substring(indexStart(ary[i], "First Name:") + 11, indexStart(ary[i], "Surname:"));
                                System.out.println("FIRST NAME: " + FIRSTNAME);
                                str[13] = FIRSTNAME;

                                SURNAME = ary[i].substring(ary[i].lastIndexOf(':') + 1);
                                System.out.println("SURNAME: " + SURNAME);
                                str[14] = SURNAME;

                                i = i + 2;//17
                                i = seachInd(i, ary, "Company name");
                                ii = seachInd(i, ary, "Street address") - 2;
                                System.out.println(ary[i]);
                                COMPANYNAME = ary[i].substring(ary[i].indexOf(':') + 1);
                                i++;
                                while (i <= ii) {
                                    COMPANYNAME += ' ' + ary[i];
                                    i++;
                                }
                                System.out.println("COMPANY NAME: " + COMPANYNAME);
                                str[15] = COMPANYNAME;

                                i = i + 1;//19
                                STREETADDRESS = ary[i].substring(ary[i].indexOf(':') + 1) + ary[i + 2].substring(0, ary[i + 2].lastIndexOf("Telephone ")) + ary[i + 4].substring(0, ary[i + 4].lastIndexOf("Mobile number:"));
                                System.out.println("STREET ADDRESS: " + STREETADDRESS);
                                str[16] = STREETADDRESS;

                                i = i + 2;//21
                                TELEPHONENUMBER = ary[i].substring(ary[i].indexOf(':') + 1);
                                System.out.println("TELEPHONE NUMBER: " + TELEPHONENUMBER);
                                str[17] = TELEPHONENUMBER;

                                i = i + 2;//23
                                MOBILENUMBER = ary[i].substring(ary[i].indexOf(':') + 1);
                                System.out.println("MOBILE NUMBER: " + MOBILENUMBER);
                                str[18] = MOBILENUMBER;

                                i = i + 2;//25
                                TOWN_CITY = ary[i].substring(ary[i].indexOf(':') + 1, ary[i].lastIndexOf("Fax number:"));
                                System.out.println("TOWN/CITY: " + TOWN_CITY);
                                str[19] = TOWN_CITY;
                                FAX_NUMBER = ary[i].substring(ary[i].lastIndexOf(':') + 1);
                                System.out.println("FAX NUMBER: " + FAX_NUMBER);
                                str[20] = FAX_NUMBER;

                                i = i + 2;//27
                                COUNTRY = ary[i].substring(ary[i].indexOf(':') + 1);
                                System.out.println("COUNTRY: " + COUNTRY);
                                str[21] = COUNTRY;

                                i = i + 4;//31
                                POSTCODE = ary[i].substring(ary[i].indexOf(':') + 1, 18);
                                System.out.println("POSTCODE: " + POSTCODE);
                                str[22] = POSTCODE;
                                EMAIL_ADDRESS = ary[i].substring(18);
                                System.out.println("EMAIL ADDRESS: " + EMAIL_ADDRESS);
                                str[23] = EMAIL_ADDRESS;

                            }


                            i = seachInd(0, ary, "3. Description of");
                            System.out.println("3. Description of the Proposal ");

                            i = i + 4;//51
                            PROPOSED = ary[i].substring(ary[i].indexOf(':') + 1);
                            System.out.println("PROPOSED: " + PROPOSED);
                            str[24] = PROPOSED;

                            i = seachInd(0, ary, "4. Site Address Details");

                            System.out.println("4. Site Address Details ");//i=67
                            System.out.println(i);
                            if (i > 1)
                            {
                                i = i + 4;//71
                                System.out.println(i + "dddddddddddddddd" + ary[i]);
                                HOUSE = ary[i].substring(ary[i].indexOf(':') + 1, ary[i].lastIndexOf("Suffix:"));
                                System.out.println("HOUSE: " + HOUSE);
                                str[25] = HOUSE;
                                SUFFIX = ary[i].substring(ary[i].lastIndexOf(':') + 1);
                                System.out.println("SUFFIX: " + SUFFIX);
                                str[26] = SUFFIX;
                                i = i + 2;//73
                                HOUSENAME = ary[i].substring(ary[i].lastIndexOf(':') + 1);
                                System.out.println("HOUSE NAME: " + HOUSENAME);
                                str[27] = HOUSENAME;
                                i = i + 2;//75
                                STREETADDRESS3 = ary[i].substring(ary[i].lastIndexOf(':') + 1) + ary[i + 2] + ary[i + 4];
                                System.out.println("STREETADDRESS3: " + STREETADDRESS3);
                                str[28] = STREETADDRESS3;
                                i = i + 6;//81
                                TOWN_CITY3 = ary[i].substring(ary[i].lastIndexOf(':') + 1);
                                System.out.println("TOWN_CITY: " + TOWN_CITY3);
                                str[29] = TOWN_CITY3;
                                i = i + 2;//83
                                POSTCODE3 = ary[i].substring(ary[i].lastIndexOf(':') + 1);
                                System.out.println("POSTCODE3: " + POSTCODE3);
                                str[30] = POSTCODE3;
                                i = i + 6;//89
                                EASTING = ary[i].substring(ary[i].lastIndexOf(':') + 1);
                                System.out.println("EASTING: " + EASTING);
                                str[31] = EASTING;
                                i = i + 2;//91
                                NORTHING = ary[i].substring(ary[i].lastIndexOf(':') + 1);
                                System.out.println("NORTHING: " + NORTHING);
                                str[32] = NORTHING;

                                i = i + 2;//93
                                DESCRIPTION = ary[i].substring(ary[i].lastIndexOf(':') + 1);
                                System.out.println("DESCRIPTION: " + DESCRIPTION);
                                str[33] = DESCRIPTION;
                            }


                            str[34] = file.getName();
//            System.out.println("!!!!!!!!!!!! " + i + " " + ary[i]);

                            //записывваем все полученные данные с одного файла в .csv
                            for (String aStr : str) {
                                String strok = "\"" + aStr + "\";";
                                Files.write(Paths.get(PATHcsvO), strok.getBytes(), StandardOpenOption.APPEND);
                            }
                            Files.write(Paths.get(PATHcsvO), "\n".getBytes(), StandardOpenOption.APPEND);


                        } else if (seachInd(0, ary, "1. Site Address") >= 0) {
                            System.out.println("new type!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

                            String Number, Suffix, PropertyName, AddressLine1, AddressLine2, AddressLine3, Town_city,
                                    Postcode, Easting, Northing, Description, Title, FirstName, Surname, CompanyName,
                                    AddressLine1A, AddressLine2A, AddressLine3A, Town_cityA, Country, PostcodeA,
                                    PrimaryNumber, SecondaryNumber, FaxNumber, EmailAddress, Decision, ReferenceNumbe,
                                    Date;

                            System.out.println("1. Site Address");

                            Integer i = seachInd(0, ary, "1. Site Address");

                            i = i + 1;
                            Number = ary[i].substring(ary[i].lastIndexOf("Number") + 6);
                            System.out.println("Number: " + Number);
                            strN[0] = Number;

                            i = i + 2;
                            Suffix = ary[i].substring(ary[i].lastIndexOf("Suffix") + 6);
                            System.out.println("Suffix: " + Suffix);
                            strN[1] = Suffix;

                            i = i + 2;
                            PropertyName = ary[i].substring(ary[i].lastIndexOf("PropertyName") + 14);
                            System.out.println("PropertyName: " + PropertyName);
                            strN[2] = PropertyName;

                            i = i + 2;
                            AddressLine1 = ary[i].substring(ary[i].lastIndexOf("AddressLine1") + 15);
                            System.out.println("AddressLine1: " + AddressLine1);
                            strN[3] = AddressLine1;

                            i = i + 2;
                            AddressLine2 = ary[i].substring(ary[i].lastIndexOf("AddressLine2") + 15);
                            System.out.println("AddressLine2: " + AddressLine2);
                            strN[4] = AddressLine2;

                            i = i + 2;
                            AddressLine3 = ary[i].substring(ary[i].lastIndexOf("AddressLine3") + 15);
                            System.out.println("AddressLine3: " + AddressLine3);
                            strN[5] = AddressLine3;

                            i = i + 2;
                            Town_city = ary[i].substring(ary[i].lastIndexOf("Town_city") + 10);
                            System.out.println("Town_city: " + Town_city);
                            strN[6] = Town_city;

                            i = i + 2;
                            Postcode = ary[i].substring(ary[i].lastIndexOf("Postcode") + 8);
                            System.out.println("Postcode: " + Postcode);
                            strN[7] = Postcode;

                            i = i + 4;
                            Easting = ary[i].substring(ary[i].lastIndexOf("Easting ") + 11);
                            System.out.println("Easting : " + Easting);
                            strN[8] = Easting;

                            i = i + 2;
                            Northing = ary[i].substring(ary[i].lastIndexOf("Northing ") + 12);
                            System.out.println("Northing : " + Northing);
                            strN[9] = Northing;

                            i = i + 3;
                            Description = ary[i];
                            System.out.println("Description : " + Description);
                            strN[10] = Description;

                            System.out.println("2. Applicant Details");
                            i = seachInd(0, ary, "2. Applicant Details");

                            i = i + 1;
                            System.out.println(ary[i]);
                            Title = ary[i].substring(ary[i].lastIndexOf("Title") + 6);
                            System.out.println("Title : " + Title);
                            strN[11] = Title;

                            i = i + 2;
                            System.out.println(ary[i]);
                            FirstName = ary[i].substring(ary[i].lastIndexOf("FirstName") + 11);
                            System.out.println("FirstName : " + FirstName);
                            strN[12] = FirstName;

                            i = i + 2;
                            System.out.println(ary[i]);
                            Surname = ary[i].substring(ary[i].lastIndexOf("Surname") + 8);
                            System.out.println("Surname : " + Surname);
                            strN[13] = Surname;

                            i = i + 2;
                            System.out.println(ary[i]);
                            CompanyName = ary[i].substring(ary[i].lastIndexOf("CompanyName") + 13);
                            System.out.println("CompanyName : " + CompanyName);
                            strN[14] = CompanyName;


                            i = i + 2;
                            AddressLine1A = ary[i].substring(ary[i].lastIndexOf("AddressLine1A") + 15);
                            System.out.println("AddressLine1A: " + AddressLine1A);
                            strN[15] = AddressLine1A;

                            i = i + 2;
                            AddressLine2A = ary[i].substring(ary[i].lastIndexOf("AddressLine2A") + 15);
                            System.out.println("AddressLine2A: " + AddressLine2A);
                            strN[16] = AddressLine2A;

                            i = i + 2;
                            AddressLine3A = ary[i].substring(ary[i].lastIndexOf("AddressLine3A") + 15);
                            System.out.println("AddressLine3A: " + AddressLine3A);
                            strN[17] = AddressLine3A;

                            i = i + 2;
                            Town_cityA = ary[i].substring(ary[i].lastIndexOf("Town_cityA") + 10);
                            System.out.println("Town_cityA: " + Town_cityA);
                            strN[18] = Town_cityA;

                            System.out.println("2. Applicant Details");
                            i = seachInd(i, ary, "2. Applicant Details");

                            i = i + 1;
                            Country = ary[i].substring(ary[i].lastIndexOf("Country") + 8);
                            System.out.println("Country: " + Country);
                            strN[19] = Country;

                            i = i + 2;
                            PostcodeA = ary[i].substring(ary[i].lastIndexOf("PostcodeA") + 10);
                            System.out.println("PostcodeA: " + PostcodeA);
                            strN[20] = PostcodeA;

                            i = i + 2;
                            PrimaryNumber = ary[i].substring(ary[i].lastIndexOf("PrimaryNumber") + 15);
                            System.out.println("PrimaryNumber: " + PrimaryNumber);
                            strN[21] = PrimaryNumber;

                            i = i + 2;
                            SecondaryNumber = ary[i].substring(ary[i].lastIndexOf("SecondaryNumber") + 17);
                            System.out.println("SecondaryNumber: " + SecondaryNumber);
                            strN[22] = SecondaryNumber;

                            i = i + 2;
                            FaxNumber = ary[i].substring(ary[i].lastIndexOf("FaxNumber") + 11);
                            System.out.println("FaxNumber: " + FaxNumber);
                            strN[23] = FaxNumber;

                            i = i + 2;
                            EmailAddress = ary[i].substring(ary[i].lastIndexOf("EmailAddress") + 14);
                            System.out.println("EmailAddress: " + EmailAddress);
                            strN[24] = EmailAddress;

                            System.out.println("3. Agent Details");
                            i = seachInd(i, ary, "3. Agent Details");


                            i = i + 1;
                            Title = ary[i].substring(ary[i].lastIndexOf("Title2") + 6);
                            System.out.println("Title : " + Title);
                            strN[25] = Title;

                            i = i + 2;
                            FirstName = ary[i].substring(ary[i].lastIndexOf("FirstName2") + 12);
                            System.out.println("FirstName : " + FirstName);
                            strN[26] = FirstName;

                            i = i + 2;
                            Surname = ary[i].substring(ary[i].lastIndexOf("Surname2") + 8);
                            System.out.println("Surname : " + Surname);
                            strN[27] = Surname;

                            i = i + 2;
                            CompanyName = ary[i].substring(ary[i].lastIndexOf("CompanyName2") + 14);
                            System.out.println("CompanyName : " + CompanyName);
                            strN[28] = CompanyName;


                            i = i + 2;
                            AddressLine1A = ary[i].substring(ary[i].lastIndexOf("AddressLine1A2") + 15);
                            System.out.println("AddressLine1A: " + AddressLine1A);
                            strN[29] = AddressLine1A;

                            i = i + 2;
                            AddressLine2A = ary[i].substring(ary[i].lastIndexOf("AddressLine2A2") + 15);
                            System.out.println("AddressLine2A: " + AddressLine2A);
                            strN[30] = AddressLine2A;

                            i = i + 2;
                            AddressLine3A = ary[i].substring(ary[i].lastIndexOf("AddressLine3A2") + 15);
                            System.out.println("AddressLine3A: " + AddressLine3A);
                            strN[31] = AddressLine3A;

                            i = i + 2;
                            Town_cityA = ary[i].substring(ary[i].lastIndexOf("Town_cityA2") + 10);
                            System.out.println("Town_cityA: " + Town_cityA);
                            strN[32] = Town_cityA;

                            i = i + 2;
                            Country = ary[i].substring(ary[i].lastIndexOf("Country") + 8);
                            System.out.println("Country: " + Country);
                            strN[33] = Country;

                            i = i + 2;
                            PostcodeA = ary[i].substring(ary[i].lastIndexOf("PostcodeA") + 10);
                            System.out.println("PostcodeA: " + PostcodeA);
                            strN[34] = PostcodeA;

                            i = i + 2;
                            PrimaryNumber = ary[i].substring(ary[i].lastIndexOf("PrimaryNumber") + 15);
                            System.out.println("PrimaryNumber: " + PrimaryNumber);
                            strN[35] = PrimaryNumber;

                            i = i + 2;
                            SecondaryNumber = ary[i].substring(ary[i].lastIndexOf("SecondaryNumber") + 17);
                            System.out.println("SecondaryNumber: " + SecondaryNumber);
                            strN[36] = SecondaryNumber;

                            i = i + 2;
                            FaxNumber = ary[i].substring(ary[i].lastIndexOf("FaxNumber") + 11);
                            System.out.println("FaxNumber: " + FaxNumber);
                            strN[37] = FaxNumber;

                            i = i + 2;
                            EmailAddress = ary[i].substring(ary[i].lastIndexOf("Email") + 6);
                            System.out.println("Email: " + EmailAddress);
                            strN[38] = EmailAddress;

                            System.out.println("4. Description of the Proposal");
                            i = seachInd(i, ary, "4. Description of the Proposal");

                            i = i + 3;
                            Decision = ary[i] + " " + ary[i + 1];
                            System.out.println("Decision: " + Decision);
                            strN[39] = Decision;

                            i = i + 4;
                            System.out.println(ary[i]);
                            ReferenceNumbe = ary[i] + " " + ary[i + 1];
                            System.out.println("ReferenceNumbe: " + ReferenceNumbe);
                            strN[40] = ReferenceNumbe;

                            i = i + 8;
                            System.out.println(ary[i]);
                            Date = ary[i];
                            System.out.println("Date: " + Date);
                            strN[41] = Date;

                            strN[42] = file.getName();

                            //записывваем все полученные данные с одного файла в .csv
                            for (String aStr : strN) {
                                String strok = "\"" + aStr + "\";";
                                Files.write(Paths.get(PATHcsvN), strok.getBytes(), StandardOpenOption.APPEND);
                            }
                            Files.write(Paths.get(PATHcsvN), "\n".getBytes(), StandardOpenOption.APPEND);
                        }
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (SAXException e) {
                    e.printStackTrace();
                } catch (TikaException e) {
                    e.printStackTrace();
                }
            }
        }
        long finish = System.currentTimeMillis();
        long timeConsumedMillis = finish - start;
        // и выводим результаты
        System.out.println("Time operating of the program(ms): " + timeConsumedMillis + " ms");
        String time = String.format("%d:%d", timeConsumedMillis / 60000, (timeConsumedMillis % 60000) / 1000);
        System.out.println("Time operating of the program(m:s): " + time);
        System.out.println("Files=" + kolF);
    }
}